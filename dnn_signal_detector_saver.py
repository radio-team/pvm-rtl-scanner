import keras
from rtlsdr import RtlSdr
import numpy
from collections import defaultdict
from datetime import datetime

class DnnDetector():
	def __init__(self, model_name, args):
		self.model = keras.models.load_model(model_name + '.model')
		self.model.load_weights(model_name + '.weights')
		self.parse_arguments(args)
		self.delta = 0.25
		self.configure_device(args)
		# TODO: unharcode
		self.samples = 2048
		# CONFIGURATION: label ratio for FM
		self.fm_noise_ratio = 1.0 / 3.0
		self.chunks_count = 10

	def get_label_indexes(self):
		return ['NOISE', 'FM', 'GSM', 'CARRIER']

	def parse_arguments(self, args):
		self.freq_correction = args.ppm
		self.gain = args.gain
		self.min_freq = args.min_freq
		self.max_freq = args.max_freq

	def configure_device(self, args):
		self.sdr = RtlSdr()
		self.sdr.sample_rate = self.delta * 1e6
		self.sdr.freq_correction = self.freq_correction
		self.sdr.gain = self.gain

	def prediction_string(self, prediction):
		max = numpy.argmax(prediction[0], axis=None)
		return self.get_label_indexes()[max]

	def read_samples(self):
		# TODO: Check whether to read garbage data
		samples = self.sdr.read_samples(self.samples * self.chunks_count)
		return samples

	def max_occurrences(self, predictions):
		d = defaultdict(int)
		for i in predictions:
			d[i] += 1
		result = max(d.items(), key=lambda x: x[1])
		return result

	def occurrences_of(self, label, predictions):
		result = 0
		for i in predictions:
			if (i == label):
				result = result + 1
		return result

	def final_label(self, predictions):
		# Method with parameterization for label definition.
		# Answer the label and the rule used.
		#  Strategy 1       :  1/3 of FM and noise is FM.
		#  (else) Strategy 2:  Majority.

		#label = 'UNKNOWN'
		values = numpy.unique(numpy.asarray(predictions))

		# Check strategy 1
		if (len(values)==2):
			times_fm = self.occurrences_of('FM', predictions)
			times_noise = self.occurrences_of('NOISE', predictions)

			if (times_noise == 0):
				label = self.max_occurrences(predictions)
				return label[0], 2
			elif ((times_fm / times_noise) > self.fm_noise_ratio):
				return 'FM', 1

			return 'NOISE', 1
		# Check strategy 2
		else:
			label = self.max_occurrences(predictions)
			return label[0], 2

		return label, 0

	def save_debug_data(self, freq, final_label, rule, predictions, samples):
		date = datetime.now().strftime("%Y%m%d%H%M%S")
		file_name = 'dbg_' + date + '_' + str(freq) + '_' + final_label + '.raw'
		# Save time data (all chunks)
		# TODO: Check if read-save more data!
		# TODO: Save fft data
		# TODO: Save data collected
		file = open(file_name, "w")

		# TODO: save data as the capturer does? Check.
		
		print(samples)

		for sample_index in range(len(samples)):
			sample = samples[sample_index]
			ba = bytearray(struct.pack("f", numpy.float32(sample.real)))
			for b in ba:
				fh.write(bytearray([b]))


		#for sample in x:
		#	ba = bytearray(struct.pack("f", numpy.float32(sample.real)))
		#		for b in ba:
		#			fh.write(bytearray([b]))
		#for sample in x:
		#	ba = bytearray(struct.pack("f", numpy.float32(sample.imag)))
		#	for b in ba:
		#		fh.write(bytearray([b]))

		# TODO: save data as the capturer does? Check.
		for sample in x:
			ba = bytearray(struct.pack("f", numpy.float32(sample.real)))
				for b in ba:
					fh.write(bytearray([b]))
			ba = bytearray(struct.pack("f", numpy.float32(sample.imag)))
				for b in ba:
					fh.write(bytearray([b]))

		file.close()
		exit(1)

	def detect(self):
		freq = self.min_freq
		while (freq < self.max_freq):
			self.sdr.center_freq = freq * 1e6
			samples = self.read_samples()
			prediction_string = ""
			predictions = []

			for i in range(self.chunks_count):
				ix = i * self.samples
				input_real = numpy.real(samples[ix:ix + self.samples])
				input_imag = numpy.imag(samples[ix:ix + self.samples])
				dnn_input = numpy.zeros((1, 2, self.samples), numpy.float32)
				# TODO: Improve this lame shit with network normalization
				adjustment = 10000.0
				dnn_input[0:0:] = input_real * adjustment
				dnn_input[0:1:] = input_imag * adjustment
				# TODO: Delete debug stuff
				result = self.model.predict(dnn_input)

				prediction_string = prediction_string + " - " + self.prediction_string(result)
				predictions.append(self.prediction_string(result))

			final_label, rule = self.final_label(predictions)
			print("Predicted " + str(freq) + " (" +  str(rule) + "): " + str(final_label) + " -> " + prediction_string)
			self.save_debug_data(freq, final_label, rule, predictions, samples)
			freq = freq + self.delta

		self.sdr.close()
		return None
