# Just check for errors in each mode
python pvm-rtl-scanner.py --device /dev/null --mode spectrum-fft --miny 0 --start 400 --end 410 --outfile test-fft-40x.png --pngsize=1024x768
python pvm-rtl-scanner.py --device /dev/null --mode spectrum-psd --miny 0 --start 400 --end 410 --outfile test-psd-40x.png --pngsize=1024x768 --samples=327680
python pvm-rtl-scanner.py --device /dev/null --mode spectrum-psd-detect --start 400 --end 410 --outfile test-psd-detect-40x.png --pngsize=1024x768 --samples=327680
python pvm-rtl-scanner.py --device /dev/null --mode spectrum-psd-detect --start 70 --end 950 --outfile test-psd-detect-70-950.png --pngsize=8192x768 --samples=327680
python pvm-rtl-scanner.py --device /dev/null --mode measure --start 409100000 --outfile=test-measure-409.json

