Description
===
Python program to perform spectrum scanning operations (power, fm transmision presence, plot spectrum as fft or psd big image, detection of gsm downlink signals, and more soon!).


Command Details
===

`python pvm-rtl-scanner.py  --help`

**usage**: 
```
pvm-rtl-scanner.py [-h] --mode
                          [--tracecolor {multicolor,blue,red}] --outfile
                          --outfile OUT_FILE_NAME --device DEVICE
                          [--start MIN_FREQ] [--end MAX_FREQ] [--gain GAIN]
                          [--samples SAMPLES_TO_READ] [--miny MINY] [--maxy MAXY]
                          [--server HTTP_SERVER] 

Process spectrum operation with RTL-SDR device.

optional arguments:
  -h, --help                Show this help message and exit
  --mode {spectrum-map,spectrum-live-map}
                            Scan mode (spectrum-map just now).
  --tracecolor {multicolor,blue,red}
                            Scan mode (spectrum-map just now).
  --outfile OUT_FILE_NAME   Output file.
  --device DEVICE           Device file.
  --start MIN_FREQ          Start of scan frequency in mhz.
  --end MAX_FREQ            End of scan frequency in mhz.
  --gain GAIN               Gain (int or auto string).
  --samples SAMPLES_TO_READ Samples to read per iteration.
  --miny MINY               Power axis y min limit.
  --maxy MAXY               Power axis y max limit.
  --server		    Server to report measurement.
```

Examples:
===

`time python pvm-rtl-scanner.py --device /dev/null --mode spectrum-fft --miny 0 --outfile fft-50m-1g.png --pngsize=1024x768`
`time python pvm-rtl-scanner.py --device /dev/null --mode spectrum-psd --start 400 --end 450 --outfile psd-400-450m.png --pngsize=1024x768`
`time python pvm-rtl-scanner.py --device /dev/null --mode spectrum-psd-detect --start 70 --end 950 --outfile psd-detect-400-405m.png --pngsize=1024x768 --samples=327680`
`time python pvm-rtl-scanner.py --device /dev/null --mode measure --start 409100000 --outfile=measurement-409100k.json`

![image](uploads/5cf4fbe096152386e443598377e13aa6/image.png)
![image](uploads/f3e0af213f81561407d67c448cf85b1a/image.png)

` time python pvm-rtl-scanner.py --device /dev/null --mode spectrum-live-map --start 400 --end 450 --outfile test-400-450m.png`


Database
===

Database argument is used by database register behavior for labeled radio signals. The idea is to track presence in time of known signals and other features in the future. 
DER specification is [here](page-database-tables).

Applications
===

- [Fingerprinting signals](https://www.rtl-sdr.com/tag/rtl_power/)
- [Determine cool zones to scan radio signals]()

References
===
- [Considerations](https://forums.qrz.com/index.php?threads/rtl-2832u-r820t-sdr-as-a-spectrum-analyser.391657/)
- [dbM values](https://www.rtl-sdr.com/forum/viewtopic.php?t=368#p1006)
- [Removing DC spike from FFT](https://www.dsprelated.com/thread/6514/removing-dc-spike-from-an-fft)

Notes 
=== 
- Detect is not used properly now, model is trained for 250k/s but it is used at 2m/s. Also seems to detect gsm zones ok. 
- Spectrum singal will be cleaned (specially the line of the middle).


