from rtlsdr import RtlSdr
import numpy as np
from collections import defaultdict
from datetime import date
import json
from json import JSONEncoder
from numpy_array_encoder import NumpyArrayEncoder


class NumpyArrayEncoder(JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        return JSONEncoder.default(self, obj)


class MeasurementScanReport():
	def __init__(self, plt, args):
		self.parse_arguments(args)
		self.configure_device(args)
		self.plt = plt

	def parse_arguments(self, args):
		self.freq_correction = args.ppm
		self.gain = args.gain
		self.freq = args.min_freq
		self.gps_location = args.gps_location
		self.samples = args.samples_to_read
		self.url = 'http://' + args.server + ':8000'
		self.out_file_name = args.out_file_name

	def configure_device(self, args):
		self.sdr = RtlSdr()
		self.sdr.sample_rate = 250000
		self.sdr.freq_correction = self.freq_correction
		self.sdr.gain = self.gain

	def read_samples(self):
		return self.sdr.read_samples(self.samples)

	def get_gps_location(self):
		return self.gps_location

	def get_psd(self, samples):
		sp = self.plt
		fs = 0.25 * 2 * 10e6
		ps, freqsFound, time, imageAxis = sp.specgram(samples, Fs=fs)
		t = np.transpose(ps)
		return t

	def get_radio_info(self):
		return ''

	def report_result(self, samples):
		now = date.today().strftime("%Y%d%m%S%M%H")
		radio_info = self.get_radio_info()
		location = self.get_gps_location()
		psd_data = self.get_psd(samples)

		with open(self.out_file_name, 'w') as fp:
			encoded_np_data = json.dumps(psd_data, cls=NumpyArrayEncoder)
			parameters = {'radio_info': radio_info, 'gps_location': location, 'psd_data': encoded_np_data, 'time': now}
			json.dump(parameters, fp)

	def detect(self):
		self.sdr.center_freq = self.freq * 1e6
		samples = self.read_samples()
		self.report_result(samples)
		self.sdr.close()
		return None
