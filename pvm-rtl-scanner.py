import argparse
import matplotlib
from pylab import *
import image_mode, live_animation_mode
import single_scan_fft, single_scan_psd, continuous_scan


parser = argparse.ArgumentParser(description='Process spectrum operations with RTL-SDR device. Now just power level, more options soon.')
parser.add_argument('--mode', dest='mode', required=True, action='store', type=str, default='spectrum-fft', choices=['spectrum-fft', 'spectrum-psd', 'spectrum-psd-detect', 'spectrum-fft-live', 'spectrum-psd-live', 'measure', 'measure-client', 'detect', 'spectrum-psd-detect-dump'], help='Scan modes : spectrum-fft, spectrum-fft-live, spectrum-psd, spectrum-psd-detect, spectrum-psd-live, detect, file-psd, measure, measure-client.')
parser.add_argument('--tracecolor', dest='tracecolor', action='store', type=str, default='red', choices=['multicolor', 'blue', 'red'], help='Scan mode (spectrum-map just now).')
parser.add_argument('--outfile', dest='out_file_name', action='store', type=str, default='out.png', help='Output file.')
parser.add_argument('--device', dest='device', required=True, action='store', type=str, help='Device file.')
parser.add_argument('--start', dest='min_freq', action='store', type=float, default=70, help='Start of scan frequency in mhz.')
parser.add_argument('--end', dest='max_freq', action='store', type=float, default=1000, help='End of scan frequency in mhz.')
parser.add_argument('--gain', dest='gain', action='store', type=int, default=40, help='Gain (int or auto string).')
parser.add_argument('--samples', dest='samples_to_read', action='store', type=int, default=32 * 1024, help='Samples to read per iteration.')
parser.add_argument('--miny', dest='miny', action='store', type=float, default=-200.0, help='Power axis y min limit.')
parser.add_argument('--maxy', dest='maxy', action='store', type=float, default=200.0, help='Power axis y max limit.')
parser.add_argument('--labels', dest='labels', action='store', type=str, default='yes', choices=['yes','no'], help='Predictive radio labels.')
parser.add_argument('--ppm', dest='ppm', action='store', type=int, default=20, help='PPM frequency correction.')
parser.add_argument('--pngsize', dest='pngsize', action='store', type=str, default='640x480', help='PNG size, as string (800x600 i.e.).')
parser.add_argument('--maxtime', dest='maxtime', action='store', type=str, default='10', help='Time to collect samples for PSD graphs.')
parser.add_argument('--stats', dest='stats', action='store', type=str, default='yes', choices=['yes', 'no'], help='Output stats.')
parser.add_argument('--server', dest='server', action='store', type=str, default="", help='Server to report measurement.')
parser.add_argument('--gpslocation', dest='gps_location', action='store', type=str, default="Unknown", help='GPS location of the measurement.')
parser.add_argument('--samplerate', dest='samplerate', action='store', type=float, default=2.048e6, help='Sampe rate for the radio device.')
parser.add_argument('--filterstart', dest='filter_start', action='store', type=float, default=0.0, help='Start of fft filter (percentage).')
parser.add_argument('--filterend', dest='filter_end', action='store', type=float, default=1.0, help='End of fft filter (percentage).')

args = parser.parse_args()

# #TODO: Initializers: process connections / init database / keras

# Initialize keras if necessary
#if (args.labels == 'yes' | args.mode == 'detect'):
#	import keras


# Execute command depending on scan mode
if (args.mode == 'spectrum-fft'):
	scan = single_scan_fft.SingleScanFFT(matplotlib.pyplot, args)
	matplotlib.rcParams["figure.figsize"] = (scan.visualizator.width, scan.visualizator.height)
	scan.scan()
elif (args.mode == 'spectrum-psd'):
	scan = single_scan_psd.SingleScanPSD(matplotlib.pyplot, args)
	matplotlib.rcParams["figure.figsize"] = (scan.visualizator.width, scan.visualizator.height)
	scan.scan()
elif (args.mode == 'spectrum-psd-detect'):
	import single_scan_psd_detect
	scan = single_scan_psd_detect.SingleScanPSDDetect(matplotlib.pyplot, 'clasifier-2-1-new', args)
	matplotlib.rcParams["figure.figsize"] = (scan.visualizator.width, scan.visualizator.height)
	scan.scan()
elif (args.mode == 'spectrum-psd-detect-dump'):
	import single_scan_psd_detect_dump
	scan = single_scan_psd_detect_dump.SingleScanPSDDetectDump(matplotlib.pyplot, 'clasifier-2-1-new', args)
	matplotlib.rcParams["figure.figsize"] = (scan.visualizator.width, scan.visualizator.height)
	scan.scan()
elif (args.mode == 'spectrum-fft-live'):
	scan = continuous_scan.ContinuousScan(matplotlib.pyplot, args)
	scan.scan()
elif (args.mode == 'spectrum-psd-live'):
	scan = continuous_scan.ContinuousScan(matplotlib.pyplot, args)
	scan.scan()
elif (args.mode == 'detect'):
	import dnn_signal_detector
	detector = dnn_signal_detector.DnnDetector('clasifier-2-1-new', args)
	detector.detect()
elif (args.mode == 'measure'):
	import measurement_detector
	detector = measurement_detector.MeasurementScanReport(matplotlib.pyplot, args)
	detector.detect()
elif (args.mode == 'measure-client'):
	detector = measurement_client_detector.MeasurementClientScanReport(matplotlib.pyplot, args)
	detector.detect()
else:
	print ('Unknown scan mode')
	exit(1)
