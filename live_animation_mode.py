import visualization_mode
from datetime import datetime, time

class LiveAnimation(visualization_mode.VisualizationMode):
	def __init__(self, plt, args):
		self.parse_arguments(args)
		self.plt = plt
		self.frames = 0
		self.plt.xlabel('Frequency (MHz)')
		self.plt.ylabel('Relative power (dB)')
		self.start = datetime.now()

	def visualize(self):
		now = datetime.now()
		td = self.start - now
		self.frames = self.frames + 1
		fps = self.frames / (td.days * 24 * 3600 + td.seconds)
		self.plt.title('Live approximate power spectrum ({:+.2f} fps)'.format(fps))
		self.plt.draw()
		self.plt.pause(0.01)
		self.plt.clf()

	def parse_arguments(self, args):
		pass
