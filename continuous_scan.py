from rtlsdr import RtlSdr
import scan_mode
import live_animation_mode
from scipy import signal
import numpy as np


class ContinuousScan(scan_mode.ScanMode):
	def __init__(self, plt, args):
		self.visualizator = live_animation_mode.LiveAnimation(plt, args)
		self.parse_arguments(args)
		self.configure_device(args)
		self.plt = plt

	def parse_arguments(self, args):
		self.min_freq = args.min_freq
		self.max_freq = args.max_freq
		self.samples_to_read = args.samples_to_read
		self.tracecolor = args.tracecolor
		self.maxy = args.maxy
		self.miny = args.miny
		self.freq_correction = args.ppm

	def configure_device(self, args):
		self.sdr = RtlSdr()
		self.sdr.sample_rate = 2.048e6
		self.sdr.freq_correction = self.freq_correction
		self.sdr.gain = args.gain
		self.delta = self.sdr.sample_rate / 1.0e6
		parts = (self.max_freq - self.min_freq) / self.delta

	def plot_labels(self):
		self.plt.text(406.0, 10.0, 'police start', fontsize=10)
		self.plt.text(412.0, 10.0, 'police end', fontsize=10)

	def plot_range(self):
		# TODO: Guess this correction will be necessary
		i = self.min_freq - self.delta / 8.0
		# Use the center, from 1/8 to 7/8 of the signal
		count = self.samples_to_read
		di = int(count/8.0)
		df = int(count/8.0*7.0)
		x = np.arange(0.0, 1.0, 1.0 / count)
		self.plt.ylim(self.miny, self.maxy)

		while (i < self.max_freq):
			self.sdr.center_freq = i * 1e6
			samples = self.sdr.read_samples(self.samples_to_read)
			y = self.process_samples(samples)
			self.plot_points(i + 2.0 * x[di:df], y[di:df])
			self.plot_labels()
			#peaks = self.get_peaks(i + x[di:df]) #y[di:df])
			#self.plot_peaks(peaks, y[di:df])
			i = i + self.delta * 6.0 / 8.0

	def scan(self):
		while (True):
			self.plot_range()
			self.visualize()
        	# #TODO: Hook when maybe iterating for some time
	        #self.sdr.close()
