import single_scan_psd_detect
import datetime
import json
from numpy_array_encoder import NumpyArrayEncoder
import numpy as np
import matplotlib.pyplot as plt


class SingleScanPSDDetectDump(single_scan_psd_detect.SingleScanPSDDetect):
	# Overloaded methods to save data on different files for relabeling / retraining

	def save_samples_and_psd(self, samples, i, sp, label, time):
		band_info_file_name = str(round(i, 2)) + '_psd_data.real'
		file = open(band_info_file_name, "w")
		encoded_np_data = json.dumps(samples.real, cls=NumpyArrayEncoder)
		file.write(encoded_np_data)
		file.close()

		band_info_file_name = str(round(i, 2)) + '_psd_data.imaginary'
		file = open(band_info_file_name, "w")
		encoded_np_data = json.dumps(samples.imag, cls=NumpyArrayEncoder)
		file.write(encoded_np_data)
		file.close()

		# TODO: put this into a method to avoid code rewriting or something
		plt.figure(2)
		band_png_file_name = str(round(i, 2)).replace(',', '_') + '_psd_data_' + label +'.png'
		plt.clf()
		
        ps, freqsFound, time, imageAxis = sp.specgram(samples, Fs=self.fs(i))
		#sp.axis("off")

		# Get a portion of the spectrum if specified filter_end / filter_start
		len_fft = len(ps)
		di = int(self.filter_start * len_fft)
		df = int(self.filter_end * len_fft)
		ps = np.transpose(ps)
		filtered = []
		for i in ps:
			filtered.append(i[di:df])

		t = np.array(filtered)
		plt.imshow(10 * np.log(t), aspect='auto')
        
		plt.savefig(band_png_file_name)
		plt.figure(1)

	def save_band_label(self, i, label, time):
		band_labels_file_name = 'labels_data.txt'
		file = open(band_labels_file_name, "a")
		file.write('\n')
		file.close()
		file = open(band_labels_file_name, "a")
		file.write(str(round(i, 2)) + ';' + label + '\n')
		file.close()

	def detect(self, samples, chunks_count):
		# Just keep the value for latter saving to file
		value = super().detect(samples, chunks_count)
		self.current_detect_label = value
		return value

	def plot_points(self, x, y, i, sp):
		# Save each subplot data to relabel & adjust / train
		time = ''
		super().plot_points(x, y, i, sp)
		self.save_samples_and_psd(y, i, sp, self.current_detect_label, time)
		self.save_band_label(i, self.current_detect_label, time)
