from rtlsdr import RtlSdr
import scan_mode
import image_mode
from scipy import signal
import numpy as np


class SingleScanFFT(scan_mode.ScanMode):
	def __init__(self, plt, args):
		self.parse_arguments(args)
		self.configure_device(args)
		self.visualizator = image_mode.Image(plt, 'Frequency (MHz)', 'Relative power (dB)', args)
		self.plt = plt

	def parse_arguments(self, args):
		self.min_freq = args.min_freq
		self.max_freq = args.max_freq
		self.samples_to_read = args.samples_to_read
		self.maxy = args.maxy
		self.miny = args.miny
		self.tracecolor = args.tracecolor
		self.freq_correction = args.ppm
		self.samplerate = args.samplerate

	def configure_device(self, args):
		self.sdr = RtlSdr()
		self.sdr.sample_rate = self.samplerate
		self.sdr.freq_correction = self.freq_correction
		self.sdr.gain = args.gain
		self.delta = self.sdr.sample_rate / 1.0e6 + 0.0001
		parts = (self.max_freq - self.min_freq) / self.delta

	def scan(self):
		# TODO: Guess this correction will be necessary
		# - self.delta * (8.0 / 6.0)
		i = self.min_freq
		# Use the center, most cool part
		count = self.samples_to_read
		di = int(count / 8.0)
		df = int(count / 8.0 * 7.0)
		x = np.arange(0.0, 1.0, 1.0 / count)

		self.plt.ylim(self.miny, self.maxy)
		while (i < self.max_freq):
			self.sdr.center_freq = i * 1e6
			samples = self.sdr.read_samples(self.samples_to_read)
			y = np.fft.fft(samples)
			self.plot_points(i + x[di:df], y[di:df])

			# 0.375 is (0.8-0.1) / 2
			i = i + self.delta * 0.375

		self.sdr.close()
		self.visualize()

	def plot_points(self, x, y):
		if (self.tracecolor == 'multicolor'):
			self.plt.plot(x, np.abs(y))
		elif (self.tracecolor == 'red'):
			self.plt.plot(x, np.abs(y), color='red')
		elif (self.tracecolor == 'blue'):
			self.plt.plot(x, np.abs(y), color='blue')
