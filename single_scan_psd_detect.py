import keras
import image_mode
from rtlsdr import RtlSdr
import scan_mode
from scipy import signal
import numpy as np
from scipy.fft import fftshift
import matplotlib
import matplotlib.transforms as mtransforms
import matplotlib.patches as mpatches
from collections import defaultdict


class SingleScanPSDDetect(scan_mode.ScanMode):
	def __init__(self, plt, model_name, args):
		self.model = keras.models.load_model(model_name + '.model')
		self.model.load_weights(model_name + '.weights')
		self.parse_arguments(args)
		self.configure_device(args)
		self.delta = (self.sdr.sample_rate / 1.0e6) * (self.filter_end - self.filter_start)
		self.visualizator = image_mode.Image(plt, 'Frequency [sec]', 'Time [Hz]', args)
		self.plt = plt
		self.chunks_count = 10
		# CONFIGURATION: label ratio for FM
		self.fm_noise_ratio = 1.0 / 3.0
		self.samples = 2048

	def parse_arguments(self, args):
		self.min_freq = args.min_freq
		self.max_freq = args.max_freq
		self.samples_to_read = args.samples_to_read
		self.max_time = args.maxtime
		self.maxy = args.maxy
		self.miny = args.miny
		self.tracecolor = args.tracecolor
		self.freq_correction = args.ppm
		self.samplerate = args.samplerate

	def configure_device(self, args):
		self.sdr = RtlSdr()
		self.sdr.sample_rate = self.samplerate
		self.sdr.freq_correction = self.freq_correction
		self.sdr.gain = args.gain

	def get_label_indexes(self):
		return ['NOISE', 'FM', 'GSM', 'CARRIER']

	def scan(self):
		# Prepare array
		count = self.samples_to_read
		x = np.arange(0.0, 1.0, 1.0 / count)

		# Collecting total amount of subplots
		count = 0
		i = self.min_freq - self.filter_start
		while (i < self.max_freq):
			i = i + self.delta
			count = count + 1
		ncols = count

		# Process stuff
		fig = self.plt.figure(constrained_layout=False)
		gs0 = fig.add_gridspec(3, 1)
		gs00 = gs0[0].subgridspec(1, ncols)
		gs01 = gs0[1].subgridspec(1, 1)
		gs02 = gs0[2].subgridspec(1, 1)

		# Process each band
		count = 0
		colors = []
		i = self.min_freq - self.filter_start
		while (i < self.max_freq):
			# Read samples
			self.sdr.center_freq = i * 1e6
			samples = self.sdr.read_samples(self.samples_to_read)
			sp = fig.add_subplot(gs00[0, count])
			sp.axis("off")

			# Detect signals presence
			self.freq = i
			color = self.color_from_detection(samples)
			colors.append(color)
			self.plot_points(i + x, samples, i, sp)

			i = i + self.delta
			count = count + 1

		plot = fig.add_subplot(gs01[0, 0])
		plot.axis("off")
		gs0.set_height_ratios([0.8, 0.1, 0.1])
		plot.pcolor(np.array([ colors ]))

		turquoise_patch = mpatches.Patch(color='turquoise', label='FM')
		violet_patch = mpatches.Patch(color='violet', label='NOISE')
		yellow_patch = mpatches.Patch(color='yellow', label='GSM')
		p = fig.add_subplot(gs02[0, 0])
		p.legend(loc='upper right', ncol=5, handles=[turquoise_patch, violet_patch, yellow_patch])
		p.axis("off")

		self.plt.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=0, hspace=0)
		self.plt.tight_layout(pad=0.0)
		self.plt.suptitle("Time [sec] vs Frequency from " + str(self.min_freq) + " to " + str(self.max_freq) + " [Mhz]", fontsize="large")
		self.visualizator.title = ''
		self.plt.subplots_adjust(top=0.90)
		self.sdr.close()
		self.visualize()

	def color_from_detection(self, samples):
		main_signal = self.detect(samples, self.chunks_count)
		if (main_signal == 'GSM'):
			return 0.8
		elif (main_signal == 'FM'):
			return 0.4
		elif (main_signal == 'CARRIER'):
			return 0.2
		elif (main_signal == 'NOISE'):
			return 0.0
		return 0.0

	def plot_points(self, x, y, i, sp):
		ps, freqsFound, time, imageAxis = sp.specgram(y, Fs=self.fs(i))
		sp.axis("off")

		# Get a portion of the spectrum if specified filter_end / filter_start
		len_fft = len(ps)
		di = int(self.filter_start * len_fft)
		df = int(self.filter_end * len_fft)
		ps = np.transpose(ps)
		filtered = []
		for i in ps:
			filtered.append(i[di:df])

		t = np.array(filtered)
		sp.imshow(10 * np.log(t), aspect='auto')
		sp.set_transform(mtransforms.Affine2D().rotate_deg(90.0))
        
	def prediction_string(self, prediction):
		max = np.argmax(prediction[0], axis=None)
		return self.get_label_indexes()[max]

	def max_occurrences(self, predictions):
		d = defaultdict(int)
		for i in predictions:
			d[i] += 1
		result = max(d.items(), key=lambda x: x[1])
		return result

	def occurrences_of(self, label, predictions):
		result = 0
		for i in predictions:
			if (i == label):
				result = result + 1
		return result

	def detect(self, samples, chunks_count):
		self.sdr.center_freq = self.freq * 1e6
		prediction_string = ""
		predictions = []

		for i in range(self.chunks_count):
			ix = i * self.samples
			input_real = np.real(samples[ix:ix + self.samples])
			input_imag = np.imag(samples[ix:ix + self.samples])
			dnn_input = np.zeros((1, 2, self.samples), np.float32)
			# TODO: Improve this lame shit with network normalization
			adjustment = 10000.0
			dnn_input[0:0:] = input_real * adjustment
			dnn_input[0:1:] = input_imag * adjustment
			# TODO: Delete debug stuff
			result = self.model.predict(dnn_input)

			prediction_string = prediction_string + " - " + self.prediction_string(result)
			predictions.append(self.prediction_string(result))

		final_label, rule = self.final_label(predictions)
		print("Predicted " + str(self.freq) + " (" +  str(rule) + "): " + str(final_label) + " -> " + prediction_string)
		return final_label

	def final_label(self, predictions):
		"Metod with parameterization for label definition."
		"  - Strategy 1: 1/3 of FM and noise is FM."
		"  - Strategy 2: Majority."

		label = 'UNKNOWN'
		values = np.unique(np.asarray(predictions))

		# Check strategy 1
		if ((len(values)==2)):
			times_fm = self.occurrences_of('FM', predictions)
			times_noise = self.occurrences_of('NOISE', predictions)

			if (times_noise == 0):
				label = self.max_occurrences(predictions)
				return label[0], 2
			elif ((times_fm / times_noise) > self.fm_noise_ratio):
				return 'FM', 1

			return 'NOISE', 1
		# Check strategy 2
		else:
			label = self.max_occurrences(predictions)
			return label[0], 2

		return label, 0

	def fs(self, i):
		return self.samplerate * 1e6

