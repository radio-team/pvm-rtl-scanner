import image_mode
from rtlsdr import RtlSdr
import scan_mode
from scipy import signal
import numpy as np
from scipy.fft import fftshift
import matplotlib
import matplotlib.transforms as mtransforms


class SingleScanPSD(scan_mode.ScanMode):
	def __init__(self, plt, args):
		self.parse_arguments(args)
		self.configure_device(args)
		self.delta = (self.sdr.sample_rate / 1.0e6) * (self.filter_end - self.filter_start)
		self.visualizator = image_mode.Image(plt, 'Frequency [sec]', 'Time [Hz]', args)
		self.plt = plt

	def parse_arguments(self, args):
		self.min_freq = args.min_freq
		self.max_freq = args.max_freq
		self.samples_to_read = args.samples_to_read
		self.max_time = args.maxtime
		self.maxy = args.maxy
		self.miny = args.miny
		self.tracecolor = args.tracecolor
		self.freq_correction = args.ppm
		self.samplerate = args.samplerate
		self.filter_start = args.filter_start
		self.filter_end = args.filter_end

	def configure_device(self, args):
		self.sdr = RtlSdr()
		self.sdr.sample_rate = self.samplerate
		self.sdr.freq_correction = self.freq_correction
		self.sdr.gain = args.gain

	def scan(self):
		# Prepare array
		count = self.samples_to_read
		x = np.arange(0.0, 1.0, 1.0 / count)

		# Collecting total amount of subplots
		count = 0
		i = self.min_freq - self.filter_start
		while (i < self.max_freq):
			i = i + self.delta
			count = count + 1
		ncols = count
		fig, axs = self.plt.subplots(ncols=ncols, nrows=1)

		if (count == 1):
			axs = [axs]
		# Process each band
		self.plt.ylim(self.miny, self.maxy)
		count = 0
		i = self.min_freq - self.filter_start
		while (i < self.max_freq):
			self.sdr.center_freq = i * 1e6
			samples = self.sdr.read_samples(self.samples_to_read)
			self.plot_points(i + x, samples, i, axs[count])

			i = i + self.delta
			count = count + 1
			#if (count==1):
			#	axs[count].axis("on")

		# Adjust final details
		self.plt.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=0, hspace=0)
		self.plt.tight_layout(pad=0.0)
		self.plt.suptitle("Time [sec] vs Frequency from " + str(self.min_freq) + " to " + str(self.max_freq) + " [Mhz]", fontsize="large")
		self.visualizator.title = ''
		self.plt.subplots_adjust(top=0.90)
		self.sdr.close()
		# Process visualization output
		self.visualize()

	def fs(self, i):
		return self.samplerate * 1e6

	def plot_points(self, x, y, i, sp):
		ps, freqsFound, time, imageAxis = sp.specgram(y, Fs=self.fs(i))
		sp.axis("off")

		# Get a portion of the spectrum if specified filter_end / filter_start
		len_fft = len(ps)
		di = int(self.filter_start * len_fft)
		df = int(self.filter_end * len_fft)
		ps = np.transpose(ps)
		filtered = []
		for i in ps:
			filtered.append(i[di:df])

		t = np.array(filtered)
		sp.imshow(10 * np.log(t), aspect='auto')
		sp.set_transform(mtransforms.Affine2D().rotate_deg(90.0))
