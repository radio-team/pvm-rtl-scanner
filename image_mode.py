import visualization_mode

class Image(visualization_mode.VisualizationMode):
	def __init__(self, plt, xtitle, ytitle, args):
		self.parse_arguments(args)
		self.plt = plt
		self.width, self.height = self.parse_image_size(args.pngsize)
		self.xtitle = xtitle
		self.ytitle = ytitle
		self.title = 'Power approximation'

	def parse_image_size(self, pngsize):
		splited = pngsize.split('x')
		return int(splited[0]) / 100.0, int(splited[1]) / 100.0

	def parse_arguments(self, args):
		self.out_file_name = args.out_file_name
		self.pngsize = args.pngsize

	def visualize(self):
		self.plt.title(self.title)
		self.plt.xlabel(self.xtitle)
		self.plt.ylabel(self.ytitle)
		self.plt.savefig(self.out_file_name)

