import numpy as np
import scipy.signal as signal
from scipy import stats


class ScanMode:
	def __init__(self, args):
		args = self.parse_arguments(parser)

	def visualize(self):
		self.visualizator.visualize()

	# TODO: Noise floor answer method
	def get_noise_level(self, samples):
		pass

	# TODO: Noise floor answer method
	def register_noise_level(self, samples):
		#noise = stats.signaltonoise(samples)
		#print("Noise level: " + str(noise))
		#exit(1)
		pass

	def get_peaks(self, samples):
		return signal.find_peaks(samples, height=1, distance=3)

	def plot_peaks(self, x, y):
		ys = []
		for y1 in range(len(x)):
			i = x[y1]
			ys.append(y[y1])
		self.plt.plot(x, ys, color='red', marker='o')

	def process_samples(self, samples):
		s = samples
		return np.fft.fft(s)

