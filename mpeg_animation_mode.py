import time
import visualization_mode
from matplotlib.pyplot import ion

class LiveAnimation(visualization_mode.VisualizationMode):
	def __init__(self, plt, args):
		self.parse_arguments(args)
		self.plt = plt
		# Working
		#self.animation = plt.FuncAnimation(fig, func=animation_frame, 
		#	frames=np.arrange(0, 10, 0.01), interval=10)
		self.plt.title('Live approximage power spectrum')
		self.plt.xlabel('Frequency (MHz)')
		self.plt.ylabel('Relative power (dB)')
		#self.plt.show(block=False)

	def visualize(self):
		self.plt.draw()
		#self.plt.show(block=False)
		self.plt.pause(0.01)
		#ion()
		print("Frame")

	def parse_arguments(self, args):
		pass
