import keras
from rtlsdr import RtlSdr
import numpy
from collections import defaultdict


class DnnDetector():
	def __init__(self, model_name, args):
		self.model = keras.models.load_model(model_name + '.model')
		self.model.load_weights(model_name + '.weights')
		self.parse_arguments(args)
		self.delta = 0.25
		self.configure_device(args)
		# TODO: unharcode
		self.samples = 2048
		# CONFIGURATION: label ratio for FM
		self.fm_noise_ratio = 1.0 / 3.0
		self.chunks_count = 10

	def get_label_indexes(self):
		return ['NOISE', 'FM', 'GSM', 'CARRIER']

	def parse_arguments(self, args):
		self.freq_correction = args.ppm
		self.gain = args.gain
		self.min_freq = args.min_freq
		self.max_freq = args.max_freq

	def configure_device(self, args):
		self.sdr = RtlSdr()
		self.sdr.sample_rate = self.delta * 1e6
		self.sdr.freq_correction = self.freq_correction
		self.sdr.gain = self.gain

	def prediction_string(self, prediction):
		max = numpy.argmax(prediction[0], axis=None)
		return self.get_label_indexes()[max]

	#def print_debug_data(self, samples):
	#	print("Unique: " + str(numpy.unique(numpy.asarray(samples))))
	#	if (self.verbose = True):
	#		print("Unique: " + str(len(numpy.unique(numpy.asarray(samples)))))

	def read_samples(self):
		# TODO: Check whether to read garbage data
		samples = self.sdr.read_samples(self.samples * self.chunks_count)
		return samples

	def max_occurrences(self, predictions):
		d = defaultdict(int)
		for i in predictions:
			d[i] += 1
		result = max(d.items(), key=lambda x: x[1])
		return result

	def occurrences_of(self, label, predictions):
		result = 0
		for i in predictions:
			if (i == label):
				result = result + 1
		return result

	def final_label(self, predictions):
		"Metod with parameterization for label definition."
		"  Strategy 1       :  1/3 of FM and noise is FM."
		"  (else) Strategy 2:  Majority."

		label = 'UNKNOWN'
		values = numpy.unique(numpy.asarray(predictions))

		# Check strategy 1
		if ((len(values)==2) #&
			#((values[0]=='FM') & (values[1]=='NOISE')) |
			#((values[0]=='NOISE') & (values[1]=='FM'))):
			):
			times_fm = self.occurrences_of('FM', predictions)
			times_noise = self.occurrences_of('NOISE', predictions)

			if (times_noise == 0):
				label = self.max_occurrences(predictions)
				return label[0], 2
			elif ((times_fm / times_noise) > self.fm_noise_ratio):
				return 'FM', 1

			return 'NOISE', 1
		# Check strategy 2
		else:
			label = self.max_occurrences(predictions)
			return label[0], 2

		return label, 0

	def detect(self):
		freq = self.min_freq
		while (freq < self.max_freq):
			self.sdr.center_freq = freq * 1e6
			samples = self.read_samples()
			prediction_string = ""
			predictions = []

			for i in range(self.chunks_count):
				ix = i * self.samples
				input_real = numpy.real(samples[ix:ix + self.samples])
				input_imag = numpy.imag(samples[ix:ix + self.samples])
				dnn_input = numpy.zeros((1, 2, self.samples), numpy.float32)
				# TODO: Improve this lame shit with network normalization
				adjustment = 10000.0
				dnn_input[0:0:] = input_real * adjustment
				dnn_input[0:1:] = input_imag * adjustment
				# TODO: Delete debug stuff
				result = self.model.predict(dnn_input)

				prediction_string = prediction_string + " - " + self.prediction_string(result)
				predictions.append(self.prediction_string(result))

			final_label, rule = self.final_label(predictions)
			print("Predicted " + str(freq) + " (" +  str(rule) + "): " + str(final_label) + " -> " + prediction_string)
			freq = freq + self.delta

		self.sdr.close()
		return None
